﻿using System;
using System.IO;

namespace ZipHeaderViewerLib.Lib.Common
{
    public interface IZipStructure
    {
        void SetSignature(UInt32 signature);
        void Read(BinaryReader reader, bool isReadSignature);
    }
}
