﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZipHeaderViewerLib.Lib.Common
{
    public static class GeneralPurposeBitFlagUtil
    {
        public static bool GetBit(UInt16 n, int bit)
        {
            return ((n & (1 << bit)) != 0);
        }

        public static void SetBit(ref UInt16 n, int bit, bool value)
        {
            if (value)
            {
                n &= (UInt16)((~(1 << bit)) & 0xFFFF);
            }
            else
            {
                n |= (UInt16)(1 << bit);
            }
        }

        public static bool GetDataDescriptorFlag(UInt16 n)
        {
            return GetBit(n, 3);
        }

        public static void SetDataDescriptorFlag(ref UInt16 n, bool value)
        {
            SetBit(ref n, 3, value);
        }

        public static bool GetLanguageEncodingFlag(UInt16 n)
        {
            return GetBit(n, 11);
        }

        public static void SetLanguageEncodingFlag(ref UInt16 n, bool value)
        {
            SetBit(ref n, 11, value);
        }
    }
}
