﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZipHeaderViewerLib.Lib.Common
{
    public static class Signature
    {
        /// <summary>
        /// PK0304
        /// </summary>
        public static readonly UInt32 LOCAL_FILE_HEADER = (UInt32)0x04034B50;
        /// <summary>
        /// PK0102
        /// </summary>
        public static readonly UInt32 CENTRAL_DIRECTORY_HEADER = (UInt32)0x02014B50;
        /// <summary>
        /// PK0506
        /// </summary>
        public static readonly UInt32 END_OF_CENTRAL_DIRECTORY_RECORD = (UInt32)0x06054B50;

        /// <summary>
        /// PK0505
        /// </summary>
        public static readonly UInt32 DIGITAL_SIGNATURE = (UInt32)0x05054B50;

        /// <summary>
        /// PK0608
        /// </summary>
        public static readonly UInt32 ARCHIVE_EXTRA_DATA_RECORD = (UInt32)0x08064B50;

        /// <summary>
        /// PK0606
        /// </summary>
        public static readonly UInt32 ZIP64_END_OF_CENTRAL_DIRECTORY_RECORD = (UInt32)0x06064B50;

        /// <summary>
        /// PK0607
        /// </summary>
        public static readonly UInt32 ZIP64_END_OF_CENTRAL_DIRECTORY_LOCATOR = (UInt32)0x07064B50;
    }
}
