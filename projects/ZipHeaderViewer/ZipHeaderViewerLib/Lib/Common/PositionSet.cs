﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZipHeaderViewerLib.Lib.Common
{
    public class PositionSet<T>
    {
        public long Position { get; set; }
        public T Value { get; private set; }

        public PositionSet(T value)
        {
            Value = value;
        }
    }
}
