﻿using System;
using System.IO;
using System.Text;

namespace ZipHeaderViewerLib.Lib.Common.Structure
{
    /// <summary>
    /// PK0608(0x08064B50)
    /// </summary>
    public class ZipArchiveExtraDataRecord : IZipStructure
    {
        public UInt32 ArchiveExtraDataSignature;
        public UInt32 ExtraFieldLength;
        public byte[] ExtraFieldData;

        public void SetSignature(UInt32 signature)
        {
            ArchiveExtraDataSignature = signature;
        }

        public void Read(BinaryReader reader, bool isReadSignature)
        {
            if (isReadSignature)
            {
                ArchiveExtraDataSignature = reader.ReadUInt32();
            }
            ExtraFieldLength = reader.ReadUInt32();
            ExtraFieldData = reader.ReadBytes((int)ExtraFieldLength);
        }
    }
}
