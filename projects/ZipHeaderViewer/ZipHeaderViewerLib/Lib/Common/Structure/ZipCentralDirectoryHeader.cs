﻿using System;
using System.IO;
using System.Text;

namespace ZipHeaderViewerLib.Lib.Common.Structure
{
    /// <summary>
    /// PK0102(0x02014B50)
    /// </summary>
    public class ZipCentralDirectoryHeader : IZipStructure
    {
        public UInt32 CentralFileHeaderSignature;
        public UInt16 VersionMadeBy;
        public UInt16 VersionNeededToExtract;
        public UInt16 GeneralPurposeBitFlag;
        public UInt16 CompressionMethod;
        public UInt16 LastModFileTime;
        public UInt16 LastModFileDate;
        public UInt32 CRC32;
        public UInt32 CompressedSize;
        public UInt32 UncompressedSize;
        public UInt16 FileNameLength;
        public UInt16 ExtraFieldLength;
        public UInt16 FileCommentLength;
        public UInt16 DiskNumberStart;
        public UInt16 InternalFileAttributes;
        public UInt32 ExternalFileAttributes;
        public UInt32 RelativeOffsetOfLocalHeader;

        public byte[] FileName;
        public byte[] ExtraField;
        public byte[] FileComment;

        public bool DataDescriptorFlag
        {
            get
            {
                return GeneralPurposeBitFlagUtil.GetDataDescriptorFlag(GeneralPurposeBitFlag);
            }
            set
            {
                GeneralPurposeBitFlagUtil.SetDataDescriptorFlag(ref GeneralPurposeBitFlag, value);
            }
        }

        public bool LanguageEncodingFlag
        {
            get
            {
                return GeneralPurposeBitFlagUtil.GetLanguageEncodingFlag(GeneralPurposeBitFlag);
            }
            set
            {
                GeneralPurposeBitFlagUtil.SetLanguageEncodingFlag(ref GeneralPurposeBitFlag, value);
            }
        }

        public DateTime GetLastModFile()
        {
            return DosDateTimeUtils.GetDateTime(LastModFileDate, LastModFileTime);
        }

        public void SetSignature(UInt32 signature)
        {
            CentralFileHeaderSignature = signature;
        }

        public void Read(BinaryReader reader, bool isReadSignature)
        {
            if (isReadSignature)
            {
                CentralFileHeaderSignature = reader.ReadUInt32();
            }
            VersionMadeBy = reader.ReadUInt16();
            VersionNeededToExtract = reader.ReadUInt16();
            GeneralPurposeBitFlag = reader.ReadUInt16();
            CompressionMethod = reader.ReadUInt16();
            LastModFileTime = reader.ReadUInt16();
            LastModFileDate = reader.ReadUInt16();
            CRC32 = reader.ReadUInt32();
            CompressedSize = reader.ReadUInt32();
            UncompressedSize = reader.ReadUInt32();
            FileNameLength = reader.ReadUInt16();
            ExtraFieldLength = reader.ReadUInt16();
            FileCommentLength = reader.ReadUInt16();
            DiskNumberStart = reader.ReadUInt16();
            InternalFileAttributes = reader.ReadUInt16();
            ExternalFileAttributes = reader.ReadUInt32();
            RelativeOffsetOfLocalHeader = reader.ReadUInt32();

            FileName = reader.ReadBytes((int)FileNameLength);
            ExtraField = reader.ReadBytes((int)ExtraFieldLength);
            FileComment = reader.ReadBytes((int)FileCommentLength);
        }
    }
}
