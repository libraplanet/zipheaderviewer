﻿using System;
using System.IO;
using System.Text;

namespace ZipHeaderViewerLib.Lib.Common.Structure
{
    /// <summary>
    /// PK0506(0x06054B50)
    /// </summary>
    public class ZipEndOfCentralDirectoryRecord : IZipStructure
    {
        public UInt32 EndOfCentralDirSignature;
        public UInt16 NumberOfThisDisk;
        public UInt16 NumberOfTheDiskWithTheStartOfTheCentralDirectory;
        public UInt16 TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk;
        public UInt16 TotalNumberOfEntriesInTheCentralDirectory;
        public UInt32 SizeOfTheCentralDirectory;
        public UInt32 OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber;

        public UInt16 ZipFileCommentLength;
        public byte[] ZipFileComment;

        public void SetSignature(UInt32 signature)
        {
            EndOfCentralDirSignature = signature;
        }

        public void Read(BinaryReader reader, bool isReadSignature)
        {
            if (isReadSignature)
            {
                EndOfCentralDirSignature = reader.ReadUInt32();
            }
            NumberOfThisDisk = reader.ReadUInt16();
            NumberOfTheDiskWithTheStartOfTheCentralDirectory = reader.ReadUInt16();
            TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk = reader.ReadUInt16();
            TotalNumberOfEntriesInTheCentralDirectory = reader.ReadUInt16();
            SizeOfTheCentralDirectory = reader.ReadUInt32();
            OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber = reader.ReadUInt32();

            ZipFileCommentLength = reader.ReadUInt16();
            ZipFileComment = reader.ReadBytes((int)ZipFileCommentLength);
        }
    }
}
