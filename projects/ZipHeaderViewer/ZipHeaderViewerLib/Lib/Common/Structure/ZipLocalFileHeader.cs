﻿using System;
using System.IO;
using System.Text;

namespace ZipHeaderViewerLib.Lib.Common.Structure
{
    /// <summary>
    /// PK0304(0x04034B50)
    /// </summary>
    public class ZipLocalFileHeader : IZipStructure
    {
        public UInt32 LocalFileHeaderSignature;
        public UInt16 VersionNeededToExtract;
        public UInt16 GeneralPurposeBitFlag;
        public UInt16 CompressionMethod;
        public UInt16 LastModFileTime;
        public UInt16 LastModFileDate;
        public UInt32 CRC32;
        public UInt32 CompressedSize;
        public UInt32 UncompressedSize;
        public UInt16 FileNameLength;
        public UInt16 ExtraFieldLength;

        public UInt32 DataDescriptorCRC32;
        public UInt32 DataDescriptorCompressedSize;
        public UInt32 DataDescriptorUncompressedSize;

        public byte[] FileName;
        public byte[] ExtraField;

        public bool DataDescriptorFlag
        {
            get
            {
                return GeneralPurposeBitFlagUtil.GetDataDescriptorFlag(GeneralPurposeBitFlag);
            }
            set
            {
                GeneralPurposeBitFlagUtil.SetDataDescriptorFlag(ref GeneralPurposeBitFlag, value);
            }
        }

        public bool LanguageEncodingFlag
        {
            get
            {
                return GeneralPurposeBitFlagUtil.GetLanguageEncodingFlag(GeneralPurposeBitFlag);
            }
            set
            {
                GeneralPurposeBitFlagUtil.SetLanguageEncodingFlag(ref GeneralPurposeBitFlag, value);
            }
        }

        public DateTime GetLastModFile()
        {
            return DosDateTimeUtils.GetDateTime(LastModFileDate, LastModFileTime);
        }

        public void SetSignature(UInt32 signature)
        {
            LocalFileHeaderSignature = signature;
        }

        public void Read(BinaryReader reader, bool isReadSignature)
        {
            if (isReadSignature)
            {
                LocalFileHeaderSignature = reader.ReadUInt32();
            }
            VersionNeededToExtract = reader.ReadUInt16();
            GeneralPurposeBitFlag = reader.ReadUInt16();
            CompressionMethod = reader.ReadUInt16();
            LastModFileTime = reader.ReadUInt16();
            LastModFileDate = reader.ReadUInt16();
            CRC32 = reader.ReadUInt32();
            CompressedSize = reader.ReadUInt32();
            UncompressedSize = reader.ReadUInt32();
            FileNameLength = reader.ReadUInt16();
            ExtraFieldLength = reader.ReadUInt16();

            FileName = reader.ReadBytes((int)FileNameLength);
#if DEBUG
            String s = System.Text.Encoding.UTF8.GetString(FileName);
#endif
            ExtraField = reader.ReadBytes((int)ExtraFieldLength);
        }

        public void ReadDataDescriptorRecord(BinaryReader reader)
        {
            DataDescriptorCRC32 = reader.ReadUInt32();
            DataDescriptorCompressedSize = reader.ReadUInt32();
            DataDescriptorUncompressedSize = reader.ReadUInt32();
        }
    }
}
