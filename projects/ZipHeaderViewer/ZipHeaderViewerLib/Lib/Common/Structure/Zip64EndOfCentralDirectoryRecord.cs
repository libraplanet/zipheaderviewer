﻿using System;
using System.IO;
using System.Text;

namespace ZipHeaderViewerLib.Lib.Common.Structure
{
    /// <summary>
    /// PK0606(0x06064B50)
    /// </summary>
    public class Zip64EndOfCentralDirectoryRecord : IZipStructure
    {
        public UInt32 Zip64EndOfCentralDirSignature;
        public UInt64 SizeOfZip64EndOfCentralDirectoryRecord;
        public UInt16 VersionMadeBy;
        public UInt16 VersionNeededToExtract;
        public UInt32 NumberOfThisDisk;
        public UInt32 NumberOfTheDiskWithTheStartOfTheCentralDirectory;
        public UInt64 TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk;
        public UInt64 TotalNumberOfEntriesInTheCentralDirectory;
        public UInt64 SizeOfTheCentralDirectory;
        public UInt64 OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber;
        public byte[] Zip64ExtensibleDataSector;

        public void SetSignature(UInt32 signature)
        {
            Zip64EndOfCentralDirSignature = signature;
        }

        //public void Read(BufferedStream stream, bool isReadSignature)
        //{
        //    using (BinaryReader bReader = new BinaryReader(stream))
        //    {
        //        if (isReadSignature)
        //        {
        //            Zip64EndOfCentralDirSignature = bReader.ReadUInt32();
        //        }
        //        SizeOfZip64EndOfCentralDirectoryRecord = bReader.ReadUInt64();
        //        VersionMadeBy = bReader.ReadUInt16();
        //        VersionNeededToExtract = bReader.ReadUInt16();
        //        NumberOfThisDisk = bReader.ReadUInt32();
        //        NumberOfTheDiskWithTheStartOfTheCentralDirectory = bReader.ReadUInt32();
        //        TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk = bReader.ReadUInt64();
        //        TotalNumberOfEntriesInTheCentralDirectory = bReader.ReadUInt64();
        //        SizeOfTheCentralDirectory = bReader.ReadUInt64();
        //        OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber = bReader.ReadUInt64();
        //        Zip64ExtensibleDataSector = bReader.ReadUInt64();
        //    }
        //}

        public void Read(BinaryReader reader, bool isReadSignature)
        {
            throw new NotImplementedException();
        }
    }
}
