﻿using System;
using System.IO;
using System.Text;

namespace ZipHeaderViewerLib.Lib.Common.Structure
{
    /// <summary>
    /// PK0607(0x07064B50)
    /// </summary>
    public class Zip64EndOfCentralDirectoryLocator : IZipStructure
    {
        public UInt32 Zip64EndOfCentralDirLocatorSignature;
        public UInt32 NumberOfTheDiskWithTheStartOfTheZip64EndOfCentralDirectory;
        public UInt64 RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord;
        public UInt32 TotalNumberOfDisks;

        public void SetSignature(UInt32 signature)
        {
            Zip64EndOfCentralDirLocatorSignature = signature;
        }

        public void Read(BinaryReader reader, bool isReadSignature)
        {
            if (isReadSignature)
            {
                Zip64EndOfCentralDirLocatorSignature = reader.ReadUInt32();
            }
            NumberOfTheDiskWithTheStartOfTheZip64EndOfCentralDirectory = reader.ReadUInt32();
            RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord = reader.ReadUInt32();
            RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord = reader.ReadUInt64();
            TotalNumberOfDisks = reader.ReadUInt32();
        }
    }
}
