﻿using System;
using System.IO;
using System.Text;

namespace ZipHeaderViewerLib.Lib.Common.Structure
{
    /// <summary>
    /// PK0505(0x05054B50)
    /// </summary>
    public class ZipDigitalSignature : IZipStructure
    {
        public UInt32 HeaderSignature;
        public UInt16 SizeOfData;
        public byte[] SignatureData;

        public void SetSignature(UInt32 signature)
        {
            HeaderSignature = signature;
        }

        public void Read(BinaryReader reader, bool isReadSignature)
        {
            if (isReadSignature)
            {
                HeaderSignature = reader.ReadUInt32();
            }
            SizeOfData = reader.ReadUInt16();
            SignatureData = reader.ReadBytes((int)SizeOfData);
        }
    }
}
