﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace ZipHeaderViewerLib.Lib.Common
{
    public static class DosDateTimeUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hour"></param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        /// <returns></returns>
        public static UInt16 GetDosTime(int hour, int min, int sec)
        {
            //opt
            {
                hour %= 24;
                min %= 60;
                sec %= 60;
            }
            //ret
            {
                UInt16 ret = 0;
                ret |= (UInt16)(hour << 11);
                ret |= (UInt16)(min << 5);
                ret |= (UInt16)(sec >> 1);
                return ret;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        public static UInt16 GetDosDate(int year, int month, int day)
        {
            //opt
            {
                month %= 12;
                day %= 32;
                year -= 1980;
                //month++;
            }
            //ret
            {
                UInt16 ret = 0;
                ret |= (UInt16)(year << 9);
                ret |= (UInt16)(month << 5);
                ret |= (UInt16)(day);
                return ret;
            }
        }
        
        public static DateTime GetDateTime(UInt16 dosDate, UInt16 dosTime)
        {
            int year0 = (dosDate >> 9) & 0x007F;
            int month = (dosDate >> 5) & 0x000F;
            int day = (dosDate >> 0) & 0x0001F;
            int hour = (dosTime >> 11) & 0x001F;
            int minute = (dosTime >> 5) & 0x003F;
            int sec = (dosTime << 1) & 0x001F;
            int year = 1980 + year0;
            return new DateTime(year, month, day, hour, minute, sec); 
        }
    }
}
