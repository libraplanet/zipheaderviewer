﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZipHeaderViewerLib.Lib.Common
{
    public class ZipStructureCollection : List<PositionSet<IZipStructure>>
    {
        public List<PositionSet<IZipStructure>> GetList<T>() where T : IZipStructure
        {
            List<PositionSet<IZipStructure>> list = new List<PositionSet<IZipStructure>>();
            foreach (PositionSet<IZipStructure> o in this)
            {
                if(o.Value is T)
                {
                    list.Add(o);
                }
            }
            return list;
        }
    }
}
