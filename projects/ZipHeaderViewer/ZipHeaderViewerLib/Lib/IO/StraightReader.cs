﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using ZipHeaderViewerLib.Lib.Common;
using ZipHeaderViewerLib.Lib.Common.Structure;

namespace ZipHeaderViewerLib.Lib.IO
{
    public class StraightReader
    {
        public ZipStructureCollection Items { get; private set; }

        public StraightReader()
        {
            Items = new ZipStructureCollection();
        }

        public void Read(BufferedStream inStream)
        {
            int tmp;
            long position;
            List<PositionSet<byte>> buf = new List<PositionSet<byte>>(4);
            Dictionary<UInt32, Type> dict = new Dictionary<uint,Type>();

            dict.Add(Signature.LOCAL_FILE_HEADER, typeof(ZipLocalFileHeader));
            dict.Add(Signature.CENTRAL_DIRECTORY_HEADER, typeof(ZipCentralDirectoryHeader));
            dict.Add(Signature.END_OF_CENTRAL_DIRECTORY_RECORD, typeof(ZipEndOfCentralDirectoryRecord));

            dict.Add(Signature.DIGITAL_SIGNATURE, typeof(ZipDigitalSignature));
            dict.Add(Signature.ARCHIVE_EXTRA_DATA_RECORD, typeof(ZipArchiveExtraDataRecord));

            dict.Add(Signature.ZIP64_END_OF_CENTRAL_DIRECTORY_LOCATOR, typeof(Zip64EndOfCentralDirectoryLocator));
            dict.Add(Signature.ZIP64_END_OF_CENTRAL_DIRECTORY_RECORD, typeof(Zip64EndOfCentralDirectoryRecord));

            position = inStream.Position;
            while ((tmp = inStream.ReadByte()) >= 0)
            {
                byte b = (byte)tmp;
                PositionSet<byte> p = new PositionSet<byte>(b) { Position = position };
                if(buf.Count < 4)
                {
                    buf.Add(p);
                }
                else
                {
                    buf.RemoveAt(0);
                    buf.Add(p);
                }

                if (buf.Count >= 4)
                {
                    UInt32 t = (UInt32)((buf[3].Value << 24) | (buf[2].Value << 16) | (buf[1].Value << 8) | (buf[0].Value << 0));
                    if (dict.ContainsKey(t))
                    {
                        if (dict[t] != null)
                        {
                            BinaryReader reader = new BinaryReader(inStream);
                            IZipStructure zipStructure = (IZipStructure)Activator.CreateInstance(dict[t]);
                            zipStructure.SetSignature(t);
                            zipStructure.Read(reader, false);
                            if (zipStructure is ZipLocalFileHeader)
                            {
                                ZipLocalFileHeader header = (ZipLocalFileHeader)zipStructure;
                                inStream.Seek(header.CompressedSize, SeekOrigin.Current);
                                if (header.DataDescriptorFlag)
                                {
                                    header.ReadDataDescriptorRecord(reader);
                                }
                            }
                            Items.Add(new PositionSet<IZipStructure>(zipStructure) { Position = buf[0].Position });
                            buf.Clear();
                        }
                    }
                }

                position = inStream.Position;
            }
        }
    }
}
