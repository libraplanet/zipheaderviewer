﻿using System;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace ZipHeaderViewer
{
    using ZipHeaderViewer.ViewModel;
    using ZipHeaderViewerLib.Lib.Common;
    using ZipHeaderViewerLib.Lib.Common.Structure;
    using ZipHeaderViewerLib.Lib.IO;

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window, System.ComponentModel.INotifyPropertyChanged
    {
        private string _fileFullPath = "";
        public string FileFullPath
        {
            get { return this._fileFullPath; }
            set
            {
                if (this._fileFullPath.Equals(value)) return;
                this._fileFullPath = value;
                this.OnPropertyChanged("FileFullPath");
            }
        }

        #region[methods]
        private void fixStateStyle()
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                BorderThickness = new Thickness(6);
                this.buttonNormalize.Visibility = System.Windows.Visibility.Visible;
                this.buttonMaximize.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                BorderThickness = new Thickness(0);
                this.buttonNormalize.Visibility = System.Windows.Visibility.Collapsed;
                this.buttonMaximize.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void Message(string s)
        {
            const int MAX_LINE = 100;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("[{0}] {1}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"), s));
            using (StringReader r = new StringReader(textBox.Text))
            {
                for (int i = 0; i < MAX_LINE - 1; i++)
                {
                    sb.AppendLine(r.ReadLine());
                }
            }
            textBox.Text = sb.ToString();
        }
        #endregion

        #region [event]
        private void window_Initialized(object sender, EventArgs e)
        {
            fixStateStyle();
        }
        private void window_StateChanged(object sender, EventArgs e)
        {
            fixStateStyle();
        }
        #endregion

        #region [command]
        private void CommandCloseCommand(object sender, RoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }
        private void CommandMaximizeWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }
        private void CommandMinimizeWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }
        private void CommandRestoreWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }
        #endregion

        private void openZipFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "all file|*|zip|*.zip";
            dialog.FilterIndex = 2;
            if (dialog.ShowDialog() == true)
            {
                string path = dialog.FileName;
                openZipFile(path);
            }
            else
            {
                Message("[MainWindow.openZipFile()] cancel brows.");
            }
        }

        private string getHexString(byte[] b)
        {
            return BitConverter.ToString(b).Replace("-", string.Empty).ToUpper();
        }

        private void resetControlsGrid(ZipStructureCollection items)
        {
            Color[,] colors = new Color[2, 2]
            {
                {Color.FromRgb(0x10, 0x10, 0x60), Color.FromRgb(0x60, 0x10, 0x10)}, //明るめ
                {Color.FromRgb(0x30, 0x30, 0x60), Color.FromRgb(0x60, 0x30, 0x30)}, //暗め
            };

            for (int i = 0; i < items.Count; i++)
            {
                PositionSet<IZipStructure> zipStructureSet = items[i];
                IZipStructure zipStructure = zipStructureSet.Value;
                Grid grid = new Grid();
                StackPanel panel = new StackPanel();
                List<string[]> list = new List<string[]>();
                Func<string, UIElement> textControl = (string s) =>
                {
                    //return new TextBlock() { Text = s };

                    TextBox t = new TextBox();
                    t.Text = s;
                    t.IsReadOnly = true;
                    t.TextWrapping = TextWrapping.Wrap;
                    t.Background = new SolidColorBrush(Colors.Transparent);
                    t.Foreground = this.Foreground;
                    t.BorderThickness = new Thickness(0);
                    return t;
                };
                Action<string, string> addList = (string cap, string val) =>
                {
                    list.Add(new string[] { cap, val });
                };

                addList(string.Format("[{0:000}]", i), "");
                addList("position", string.Format("{0:#,##0}(0x{1:X8})", new object[] { zipStructureSet.Position, zipStructureSet.Position }));

                if (zipStructure is ZipLocalFileHeader)
                {
                    ZipLocalFileHeader o = (ZipLocalFileHeader)zipStructure;
                    DateTime lastModFile = o.GetLastModFile();
                    Encoding enc;
                    if (o.LanguageEncodingFlag)
                    {
                        enc = Encoding.UTF8;
                    }
                    else
                    {
                        enc = Encoding.Default;
                    }

                    addList("signiture", string.Format("0x{0:X8}({1})", new object[] { o.LocalFileHeaderSignature, "ZipLocalFileHeader" }));
                    addList("VersionNeededToExtract", string.Format("0x{0:X8}", new object[] { o.VersionNeededToExtract }));
                    addList("GeneralPurposeBitFlag", string.Format("0x{0:X8}", new object[] { o.GeneralPurposeBitFlag }));
                    addList("CompressionMethod", string.Format("0x{0:X8}", new object[] { o.CompressionMethod }));
                    addList("LastModFileTime", string.Format("0x{0:X8}({1})", new object[] { o.LastModFileTime, lastModFile.ToString("yyyy/MM/dd") }));
                    addList("LastModFileDate", string.Format("0x{0:X8}({1})", new object[] { o.LastModFileDate, lastModFile.ToString("HH:mm:ss") }));
                    addList("CRC32", string.Format("0x{0:X8}", new object[] { o.CRC32 }));
                    addList("CompressedSize", string.Format("{0:#,##0}", new object[] { o.CompressedSize }));
                    addList("UncompressedSize", string.Format("{0:#,##0}", new object[] { o.UncompressedSize }));
                    addList("FileNameLength", string.Format("{0:#,##0}", new object[] { o.FileNameLength }));
                    addList("ExtraFieldLength", string.Format("{0:#,##0}", new object[] { o.ExtraFieldLength }));
                    addList("DataDescriptorCRC32", string.Format("0x{0:X8}", new object[] { o.DataDescriptorCRC32 }));
                    addList("DataDescriptorCompressedSize", string.Format("{0:#,##0}", new object[] { o.DataDescriptorCompressedSize }));
                    addList("DataDescriptorUncompressedSize", string.Format("{0:#,##0}", new object[] { o.DataDescriptorUncompressedSize }));
                    addList(string.Format("FileName({0})", enc.WebName), string.Format("{0}", new object[] { enc.GetString(o.FileName) }));
                    addList(string.Format("  SYSTEM({0})", Encoding.Default.WebName), string.Format("{0}", new object[] { Encoding.Default.GetString(o.FileName) }));
                    addList(string.Format("  UTF-8({0})", Encoding.UTF8.WebName), string.Format("{0}", new object[] { Encoding.UTF8.GetString(o.FileName) }));
                    addList(string.Format("  HEX"), string.Format("0x{0}", new object[] { getHexString(o.FileName) }));
                    addList("-------------------------", "");
                    addList("LanguageEncodingFlag", string.Format("{0}", new object[] { o.LanguageEncodingFlag }));
                }
                else if (zipStructure is ZipCentralDirectoryHeader)
                {
                    ZipCentralDirectoryHeader o = (ZipCentralDirectoryHeader)zipStructure;
                    Encoding enc;
                    DateTime lastModFile = o.GetLastModFile();
                    if (o.LanguageEncodingFlag)
                    {
                        enc = Encoding.UTF8;
                    }
                    else
                    {
                        enc = Encoding.Default;
                    }
                    addList("signiture", string.Format("0x{0:X8}({1})", new object[] { o.CentralFileHeaderSignature, "ZipCentralDirectoryHeader" }));
                    addList("VersionMadeBy", string.Format("0x{0:X8}", new object[] { o.VersionMadeBy }));
                    addList("VersionNeededToExtract", string.Format("0x{0:X8}", new object[] { o.VersionNeededToExtract }));
                    addList("GeneralPurposeBitFlag", string.Format("0x{0:X8}", new object[] { o.GeneralPurposeBitFlag }));
                    addList("CompressionMethod", string.Format("0x{0:X8}", new object[] { o.CompressionMethod }));
                    addList("LastModFileTime", string.Format("0x{0:X8}({1})", new object[] { o.LastModFileTime, lastModFile.ToString("yyyy/MM/dd") }));
                    addList("LastModFileDate", string.Format("0x{0:X8}({1})", new object[] { o.LastModFileDate, lastModFile.ToString("hh:mm:ss") }));
                    addList("CRC32", string.Format("0x{0:X8}", new object[] { o.CRC32 }));
                    addList("CompressedSize", string.Format("{0:#,##0}", new object[] { o.CompressedSize }));
                    addList("UncompressedSize", string.Format("{0:#,##0}", new object[] { o.UncompressedSize }));
                    addList("FileNameLength", string.Format("{0:#,##0}", new object[] { o.FileNameLength }));
                    addList("ExtraFieldLength", string.Format("{0:#,##0}", new object[] { o.ExtraFieldLength }));
                    addList("FileCommentLength", string.Format("{0:#,##0}", new object[] { o.FileCommentLength }));
                    addList("DiskNumberStart", string.Format("{0:#,##0}", new object[] { o.DiskNumberStart }));
                    addList("InternalFileAttributes", string.Format("0x{0:X8}", new object[] { o.InternalFileAttributes }));
                    addList("ExternalFileAttributes", string.Format("0x{0:X8}", new object[] { o.ExternalFileAttributes }));
                    addList("RelativeOffsetOfLocalHeader", string.Format("{0:#,##0}(0x{1:X8})", new object[] { o.RelativeOffsetOfLocalHeader, o.RelativeOffsetOfLocalHeader }));

                    addList(string.Format("FileName({0})", enc.WebName), string.Format("{0}", new object[] { enc.GetString(o.FileName) }));
                    addList(string.Format("  SYSTEM({0})", Encoding.Default.WebName), string.Format("{0}", new object[] { Encoding.Default.GetString(o.FileName) }));
                    addList(string.Format("  UTF-8({0})", Encoding.UTF8.WebName), string.Format("{0}", new object[] { Encoding.UTF8.GetString(o.FileName) }));
                    addList(string.Format("  HEX"), string.Format("0x{0}", new object[] { getHexString(o.FileName) }));

                    addList(string.Format("ExtraField"), string.Format("0x{0}", new object[] { getHexString(o.ExtraField) }));

                    addList(string.Format("FileComment({0})", enc.WebName), string.Format("{0}", new object[] { enc.GetString(o.FileComment) }));
                    addList(string.Format("  SYSTEM({0})", Encoding.Default.WebName), string.Format("{0}", new object[] { Encoding.Default.GetString(o.FileComment) }));
                    addList(string.Format("  UTF-8({0})", Encoding.UTF8.WebName), string.Format("{0}", new object[] { Encoding.UTF8.GetString(o.FileComment) }));
                    addList(string.Format("  HEX"), string.Format("0x{0}", new object[] { getHexString(o.FileComment) }));
                    addList("-------------------------", "");
                    addList("LanguageEncodingFlag", string.Format("{0}", new object[] { o.LanguageEncodingFlag }));
                }
                else if (zipStructure is ZipEndOfCentralDirectoryRecord)
                {
                    ZipEndOfCentralDirectoryRecord o = (ZipEndOfCentralDirectoryRecord)zipStructure;
                    addList("signiture", string.Format("0x{0:X8}({1})", new object[] { o.EndOfCentralDirSignature, "ZipEndOfCentralDirectoryRecord" }));
                    addList("NumberOfThisDisk", string.Format("{0:#,##0}", new object[] { o.NumberOfThisDisk }));
                    addList("NumberOfTheDiskWithTheStartOfTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.NumberOfTheDiskWithTheStartOfTheCentralDirectory }));
                    addList("TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk }));
                    addList("TotalNumberOfEntriesInTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfEntriesInTheCentralDirectory }));
                    addList("SizeOfTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.SizeOfTheCentralDirectory }));
                    addList("OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber", string.Format("{0:#,##0}(0x{1:X8})", new object[] { o.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber, o.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber }));
                    addList("ZipFileCommentLength", string.Format("{0:#,##0}", new object[] { o.ZipFileCommentLength }));

                    addList(string.Format("ZipFileComment"), "");
                    addList(string.Format("  SYSTEM({0})", Encoding.Default.WebName), string.Format("{0}", new object[] { Encoding.Default.GetString(o.ZipFileComment) }));
                    addList(string.Format("  UTF-8({0})", Encoding.UTF8.WebName), string.Format("{0}", new object[] { Encoding.UTF8.GetString(o.ZipFileComment) }));
                    addList(string.Format("  HEX"), string.Format("0x{0}", new object[] { getHexString(o.ZipFileComment) }));
                }
                else if (zipStructure is ZipDigitalSignature)
                {
                    ZipDigitalSignature o = (ZipDigitalSignature)zipStructure;
                    addList("signiture", string.Format("0x{0:X8}({1})", new object[] { o.HeaderSignature, "ZipDigitalSignature" }));
                    addList("SizeOfData", string.Format("{0:#,##0}", new object[] { o.SizeOfData }));
                    addList("SignatureData", string.Format("0x{0}", new object[] { getHexString(o.SignatureData) }));
                }
                else if (zipStructure is ZipArchiveExtraDataRecord)
                {
                    ZipArchiveExtraDataRecord o = (ZipArchiveExtraDataRecord)zipStructure;
                    addList("signiture", string.Format("0x{0:X8}({1})", new object[] { o.ArchiveExtraDataSignature, "ZipArchiveExtraDataRecord" }));
                    addList("ExtraFieldLength", string.Format("{0:#,##0}", new object[] { o.ExtraFieldLength }));
                    addList("ExtraFieldData", string.Format("0x{0}", new object[] { getHexString(o.ExtraFieldData) }));
                }
                else if (zipStructure is Zip64EndOfCentralDirectoryLocator)
                {
                    Zip64EndOfCentralDirectoryLocator o = (Zip64EndOfCentralDirectoryLocator)zipStructure;
                    addList("signiture", string.Format("0x{0:X8}({1})", new object[] { o.Zip64EndOfCentralDirLocatorSignature, "Zip64EndOfCentralDirectoryLocator" }));
                    addList("NumberOfTheDiskWithTheStartOfTheZip64EndOfCentralDirectory", string.Format("{0:#,##0}", new object[] { o.NumberOfTheDiskWithTheStartOfTheZip64EndOfCentralDirectory }));
                    addList("RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord", string.Format("{0:#,##0}(0x{1:X16})", new object[] { o.RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord, o.RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord }));
                    addList("TotalNumberOfDisks", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfDisks }));
                }
                else if (zipStructure is Zip64EndOfCentralDirectoryRecord)
                {
                    Zip64EndOfCentralDirectoryRecord o = (Zip64EndOfCentralDirectoryRecord)zipStructure;
                    addList("signiture", string.Format("0x{0:X8}({1})", new object[] { o.Zip64EndOfCentralDirSignature, "Zip64EndOfCentralDirectoryRecord" }));
                    addList("SizeOfZip64EndOfCentralDirectoryRecord", string.Format("{0:#,##0}", new object[] { o.SizeOfZip64EndOfCentralDirectoryRecord }));
                    addList("VersionMadeBy", string.Format("0x{0:X8}", new object[] { o.VersionMadeBy }));
                    addList("VersionNeededToExtract", string.Format("0x{0:X8}", new object[] { o.VersionNeededToExtract }));
                    addList("NumberOfThisDisk", string.Format("{0:#,##0}", new object[] { o.NumberOfThisDisk }));
                    addList("NumberOfTheDiskWithTheStartOfTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.NumberOfTheDiskWithTheStartOfTheCentralDirectory }));
                    addList("TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk }));
                    addList("TotalNumberOfEntriesInTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfEntriesInTheCentralDirectory }));
                    addList("SizeOfTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.SizeOfTheCentralDirectory }));
                    addList("OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber", string.Format("{0:#,##0}(0x{1:X16})", new object[] { o.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber, o.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber }));
                    addList("Zip64ExtensibleDataSector", string.Format("0x{0}", new object[] { getHexString(o.Zip64ExtensibleDataSector) }));
                }

                for (int j = 0; j < list.Count; j++)
                {
                    string[] l = list[j];
                    Grid[] cells = { new Grid(), new Grid() };
                    cells[0].Children.Add(textControl(l[0]));
                    cells[1].Children.Add(textControl(l[1]));
                    Grid.SetColumn(cells[0], 0);
                    Grid.SetColumn(cells[1], 1);
                    Grid.SetRow(cells[0], j);
                    Grid.SetRow(cells[1], j);
                    grid.Children.Add(cells[0]);
                    grid.Children.Add(cells[1]);

                    grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Auto) });
                    cells[0].Margin = new Thickness(5);
                    cells[1].Margin = new Thickness(5);
                    cells[1].Background = new SolidColorBrush(Color.FromRgb(0x50, 0x00, 0x00));
                }
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1.0, GridUnitType.Auto) });
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1.0, GridUnitType.Star) });
                grid.Background = new SolidColorBrush(colors[i % 2, 0]);
                grid.Margin = new Thickness(5);
                Grid.SetRow(grid, i);
                gridColntrol.Children.Add(grid);

                gridColntrol.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });
            }

        }

        private void resetControlsViewModel(ZipStructureCollection items)
        {
            Color[,] colors = new Color[2, 2]
            {
                {Color.FromRgb(0x10, 0x10, 0x60), Color.FromRgb(0x60, 0x10, 0x10)}, //明るめ
                {Color.FromRgb(0x30, 0x30, 0x60), Color.FromRgb(0x60, 0x30, 0x30)}, //暗め
            };

            for (int i = 0; i < items.Count; i++)
            {
                PositionSet<IZipStructure> zipStructureSet = items[i];
                IZipStructure zipStructure = zipStructureSet.Value;
                ZipEntryViewModel viewModel = new ZipEntryViewModel();
                string strSignitureName = "N/A";
                UInt32 nSigniture = 0xFFFFFFFF;

                Action<int, string, string> addList = (int index, string name, string value) =>
                {
                    //viewModel.SetAttributeName(index, name);
                    //viewModel.SetAttributeValue(index, value);
                    //viewModel.SetAttributeEnabled(index, true);
                    viewModel.SetAttributeRow(name, value);
                };


                if (zipStructure is ZipLocalFileHeader)
                {
                    ZipLocalFileHeader o = (ZipLocalFileHeader)zipStructure;
                    DateTime lastModFile = o.GetLastModFile();
                    Encoding enc;
                    if (o.LanguageEncodingFlag)
                    {
                        enc = Encoding.UTF8;
                    }
                    else
                    {
                        enc = Encoding.Default;
                    }

                    strSignitureName = "ZipLocalFileHeader";
                    nSigniture = o.LocalFileHeaderSignature;
                    addList(0, "signiture", string.Format("0x{0:X8}({1})", new object[] { o.LocalFileHeaderSignature, "ZipLocalFileHeader" }));
                    addList(1, "VersionNeededToExtract", string.Format("0x{0:X8}", new object[] { o.VersionNeededToExtract }));
                    addList(2, "GeneralPurposeBitFlag", string.Format("0x{0:X8}", new object[] { o.GeneralPurposeBitFlag }));
                    addList(3, "CompressionMethod", string.Format("0x{0:X8}", new object[] { o.CompressionMethod }));
                    addList(4, "LastModFileTime", string.Format("0x{0:X8}({1})", new object[] { o.LastModFileTime, lastModFile.ToString("yyyy/MM/dd") }));
                    addList(5, "LastModFileDate", string.Format("0x{0:X8}({1})", new object[] { o.LastModFileDate, lastModFile.ToString("HH:mm:ss") }));
                    addList(6, "CRC32", string.Format("0x{0:X8}", new object[] { o.CRC32 }));
                    addList(7, "CompressedSize", string.Format("{0:#,##0}", new object[] { o.CompressedSize }));
                    addList(8, "UncompressedSize", string.Format("{0:#,##0}", new object[] { o.UncompressedSize }));
                    addList(9, "FileNameLength", string.Format("{0:#,##0}", new object[] { o.FileNameLength }));
                    addList(10, "ExtraFieldLength", string.Format("{0:#,##0}", new object[] { o.ExtraFieldLength }));
                    addList(11, "DataDescriptorCRC32", string.Format("0x{0:X8}", new object[] { o.DataDescriptorCRC32 }));
                    addList(12, "DataDescriptorCompressedSize", string.Format("{0:#,##0}", new object[] { o.DataDescriptorCompressedSize }));
                    addList(13, "DataDescriptorUncompressedSize", string.Format("{0:#,##0}", new object[] { o.DataDescriptorUncompressedSize }));
                    addList(14, string.Format("FileName({0})", enc.WebName), string.Format("{0}", new object[] { enc.GetString(o.FileName) }));
                    addList(15, string.Format("  SYSTEM({0})", Encoding.Default.WebName), string.Format("{0}", new object[] { Encoding.Default.GetString(o.FileName) }));
                    addList(16, string.Format("  UTF-8({0})", Encoding.UTF8.WebName), string.Format("{0}", new object[] { Encoding.UTF8.GetString(o.FileName) }));
                    addList(17, string.Format("  HEX"), string.Format("0x{0}", new object[] { getHexString(o.FileName) }));
                    addList(18, "-------------------------", "");
                    addList(19, "LanguageEncodingFlag", string.Format("{0}", new object[] { o.LanguageEncodingFlag }));
                }
                else if (zipStructure is ZipCentralDirectoryHeader)
                {
                    ZipCentralDirectoryHeader o = (ZipCentralDirectoryHeader)zipStructure;
                    Encoding enc;
                    DateTime lastModFile = o.GetLastModFile();
                    if (o.LanguageEncodingFlag)
                    {
                        enc = Encoding.UTF8;
                    }
                    else
                    {
                        enc = Encoding.Default;
                    }
                    strSignitureName = "ZipCentralDirectoryHeader";
                    nSigniture = o.CentralFileHeaderSignature;
                    addList(0, "signiture", string.Format("0x{0:X8}({1})", new object[] { o.CentralFileHeaderSignature, "ZipCentralDirectoryHeader" }));
                    addList(1, "VersionMadeBy", string.Format("0x{0:X8}", new object[] { o.VersionMadeBy }));
                    addList(2, "VersionNeededToExtract", string.Format("0x{0:X8}", new object[] { o.VersionNeededToExtract }));
                    addList(3, "GeneralPurposeBitFlag", string.Format("0x{0:X8}", new object[] { o.GeneralPurposeBitFlag }));
                    addList(4, "CompressionMethod", string.Format("0x{0:X8}", new object[] { o.CompressionMethod }));
                    addList(5, "LastModFileTime", string.Format("0x{0:X8}({1})", new object[] { o.LastModFileTime, lastModFile.ToString("yyyy/MM/dd") }));
                    addList(6, "LastModFileDate", string.Format("0x{0:X8}({1})", new object[] { o.LastModFileDate, lastModFile.ToString("hh:mm:ss") }));
                    addList(7, "CRC32", string.Format("0x{0:X8}", new object[] { o.CRC32 }));
                    addList(8, "CompressedSize", string.Format("{0:#,##0}", new object[] { o.CompressedSize }));
                    addList(9, "UncompressedSize", string.Format("{0:#,##0}", new object[] { o.UncompressedSize }));
                    addList(10, "FileNameLength", string.Format("{0:#,##0}", new object[] { o.FileNameLength }));
                    addList(11, "ExtraFieldLength", string.Format("{0:#,##0}", new object[] { o.ExtraFieldLength }));
                    addList(12, "FileCommentLength", string.Format("{0:#,##0}", new object[] { o.FileCommentLength }));
                    addList(13, "DiskNumberStart", string.Format("{0:#,##0}", new object[] { o.DiskNumberStart }));
                    addList(14, "InternalFileAttributes", string.Format("0x{0:X8}", new object[] { o.InternalFileAttributes }));
                    addList(15, "ExternalFileAttributes", string.Format("0x{0:X8}", new object[] { o.ExternalFileAttributes }));
                    addList(16, "RelativeOffsetOfLocalHeader", string.Format("{0:#,##0}(0x{1:X8})", new object[] { o.RelativeOffsetOfLocalHeader, o.RelativeOffsetOfLocalHeader }));

                    addList(17, string.Format("FileName({0})", enc.WebName), string.Format("{0}", new object[] { enc.GetString(o.FileName) }));
                    addList(18, string.Format("  SYSTEM({0})", Encoding.Default.WebName), string.Format("{0}", new object[] { Encoding.Default.GetString(o.FileName) }));
                    addList(19, string.Format("  UTF-8({0})", Encoding.UTF8.WebName), string.Format("{0}", new object[] { Encoding.UTF8.GetString(o.FileName) }));
                    addList(20, string.Format("  HEX"), string.Format("0x{0}", new object[] { getHexString(o.FileName) }));

                    addList(21, string.Format("ExtraField"), string.Format("0x{0}", new object[] { getHexString(o.ExtraField) }));

                    addList(22, string.Format("FileComment({0})", enc.WebName), string.Format("{0}", new object[] { enc.GetString(o.FileComment) }));
                    addList(23, string.Format("  SYSTEM({0})", Encoding.Default.WebName), string.Format("{0}", new object[] { Encoding.Default.GetString(o.FileComment) }));
                    addList(24, string.Format("  UTF-8({0})", Encoding.UTF8.WebName), string.Format("{0}", new object[] { Encoding.UTF8.GetString(o.FileComment) }));
                    addList(25, string.Format("  HEX"), string.Format("0x{0}", new object[] { getHexString(o.FileComment) }));
                    addList(26, "-------------------------", "");
                    addList(27, "LanguageEncodingFlag", string.Format("{0}", new object[] { o.LanguageEncodingFlag }));
                }
                else if (zipStructure is ZipEndOfCentralDirectoryRecord)
                {
                    ZipEndOfCentralDirectoryRecord o = (ZipEndOfCentralDirectoryRecord)zipStructure;
                    strSignitureName = "ZipEndOfCentralDirectoryRecord";
                    nSigniture = o.EndOfCentralDirSignature;
                    addList(0, "signiture", string.Format("0x{0:X8}({1})", new object[] { o.EndOfCentralDirSignature, "ZipEndOfCentralDirectoryRecord" }));
                    addList(1, "NumberOfThisDisk", string.Format("{0:#,##0}", new object[] { o.NumberOfThisDisk }));
                    addList(2, "NumberOfTheDiskWithTheStartOfTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.NumberOfTheDiskWithTheStartOfTheCentralDirectory }));
                    addList(3, "TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk }));
                    addList(4, "TotalNumberOfEntriesInTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfEntriesInTheCentralDirectory }));
                    addList(5, "SizeOfTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.SizeOfTheCentralDirectory }));
                    addList(6, "OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber", string.Format("{0:#,##0}(0x{1:X8})", new object[] { o.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber, o.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber }));
                    addList(7, "ZipFileCommentLength", string.Format("{0:#,##0}", new object[] { o.ZipFileCommentLength }));

                    addList(8, string.Format("ZipFileComment"), "");
                    addList(9, string.Format("  SYSTEM({0})", Encoding.Default.WebName), string.Format("{0}", new object[] { Encoding.Default.GetString(o.ZipFileComment) }));
                    addList(10, string.Format("  UTF-8({0})", Encoding.UTF8.WebName), string.Format("{0}", new object[] { Encoding.UTF8.GetString(o.ZipFileComment) }));
                    addList(11, string.Format("  HEX"), string.Format("0x{0}", new object[] { getHexString(o.ZipFileComment) }));
                }
                else if (zipStructure is ZipDigitalSignature)
                {
                    ZipDigitalSignature o = (ZipDigitalSignature)zipStructure;
                    strSignitureName = "ZipDigitalSignature";
                    nSigniture = o.HeaderSignature;
                    addList(0, "signiture", string.Format("0x{0:X8}({1})", new object[] { o.HeaderSignature, "ZipDigitalSignature" }));
                    addList(1, "SizeOfData", string.Format("{0:#,##0}", new object[] { o.SizeOfData }));
                    addList(2, "SignatureData", string.Format("0x{0}", new object[] { getHexString(o.SignatureData) }));
                }
                else if (zipStructure is ZipArchiveExtraDataRecord)
                {
                    ZipArchiveExtraDataRecord o = (ZipArchiveExtraDataRecord)zipStructure;
                    strSignitureName = "ZipArchiveExtraDataRecord";
                    nSigniture = o.ArchiveExtraDataSignature;
                    addList(0, "signiture", string.Format("0x{0:X8}({1})", new object[] { o.ArchiveExtraDataSignature, "ZipArchiveExtraDataRecord" }));
                    addList(1, "ExtraFieldLength", string.Format("{0:#,##0}", new object[] { o.ExtraFieldLength }));
                    addList(2, "ExtraFieldData", string.Format("0x{0}", new object[] { getHexString(o.ExtraFieldData) }));
                }
                else if (zipStructure is Zip64EndOfCentralDirectoryLocator)
                {
                    Zip64EndOfCentralDirectoryLocator o = (Zip64EndOfCentralDirectoryLocator)zipStructure;
                    strSignitureName = "Zip64EndOfCentralDirectoryLocator";
                    addList(0, "signiture", string.Format("0x{0:X8}({1})", new object[] { o.Zip64EndOfCentralDirLocatorSignature, "Zip64EndOfCentralDirectoryLocator" }));
                    addList(1, "NumberOfTheDiskWithTheStartOfTheZip64EndOfCentralDirectory", string.Format("{0:#,##0}", new object[] { o.NumberOfTheDiskWithTheStartOfTheZip64EndOfCentralDirectory }));
                    addList(2, "RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord", string.Format("{0:#,##0}(0x{1:X16})", new object[] { o.RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord, o.RelativeOffsetOfTheZip64EndOfCentralDirectoryRecord }));
                    addList(3, "TotalNumberOfDisks", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfDisks }));
                }
                else if (zipStructure is Zip64EndOfCentralDirectoryRecord)
                {
                    Zip64EndOfCentralDirectoryRecord o = (Zip64EndOfCentralDirectoryRecord)zipStructure;
                    strSignitureName = "Zip64EndOfCentralDirectoryRecord";
                    nSigniture = o.Zip64EndOfCentralDirSignature;
                    addList(0, "signiture", string.Format("0x{0:X8}({1})", new object[] { o.Zip64EndOfCentralDirSignature, "Zip64EndOfCentralDirectoryRecord" }));
                    addList(1, "SizeOfZip64EndOfCentralDirectoryRecord", string.Format("{0:#,##0}", new object[] { o.SizeOfZip64EndOfCentralDirectoryRecord }));
                    addList(2, "VersionMadeBy", string.Format("0x{0:X8}", new object[] { o.VersionMadeBy }));
                    addList(3, "VersionNeededToExtract", string.Format("0x{0:X8}", new object[] { o.VersionNeededToExtract }));
                    addList(4, "NumberOfThisDisk", string.Format("{0:#,##0}", new object[] { o.NumberOfThisDisk }));
                    addList(5, "NumberOfTheDiskWithTheStartOfTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.NumberOfTheDiskWithTheStartOfTheCentralDirectory }));
                    addList(6, "TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfEntriesInTheCentralDirectoryOnThisDisk }));
                    addList(7, "TotalNumberOfEntriesInTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.TotalNumberOfEntriesInTheCentralDirectory }));
                    addList(8, "SizeOfTheCentralDirectory", string.Format("{0:#,##0}", new object[] { o.SizeOfTheCentralDirectory }));
                    addList(9, "OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber", string.Format("{0:#,##0}(0x{1:X16})", new object[] { o.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber, o.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber }));
                    addList(10, "Zip64ExtensibleDataSector", string.Format("0x{0}", new object[] { getHexString(o.Zip64ExtensibleDataSector) }));
                }

                viewModel.textHeader.Text = string.Format("[{0:000}] {1:#,##0}(0x{1:X8}) - 0x{2:X8} {3}", new object[] { i, zipStructureSet.Position, nSigniture, strSignitureName });

                //viewModel.Background = new SolidColorBrush(colors[i % 2, 0]);
                viewModel.Margin = new Thickness(5);
                Grid.SetRow(viewModel, i);
                gridColntrol.Children.Add(viewModel);

                gridColntrol.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });
            }

        }

        private void openZipFile(string path)
        {
            StraightReader reader = new StraightReader();
            try
            {
                Message(string.Format("[MainWindow.openZipFile()] load({0})", new object[] { path }));
                {
                    using (FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    using (BufferedStream bStream = new BufferedStream(fStream))
                    {
                        reader.Read(bStream);
                    }
                }
                Message(string.Format("[MainWindow.openZipFile()] loaded({0})", new object[] { path }));
            }
            catch (Exception ex)
            {
                Message(string.Format("[MainWindow.openZipFile()] {0}", new object[] { ex.ToString() }));
                Message(string.Format("[MainWindow.openZipFile()] faild...({0})", new object[] { path }));
            }
            finally
            {
                FileFullPath = path;
                gridColntrol.Children.Clear();
                resetControlsViewModel(reader.Items);
            }
        }

        #region [menu]
        private void FileMenuOpen(object sender, RoutedEventArgs e)
        {
            openZipFile();
        }

        private void WindowMenuTopMost(object sender, RoutedEventArgs e)
        {
            this.Topmost = !this.Topmost;
            menuItemTopMost.IsChecked = this.Topmost;
        }
        private void HelpMenuVersion(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("{0}", this.Title));
            sb.AppendLine();
            sb.AppendLine(string.Format("  vsersion : {0}", "0.0.0.0.0.0.0.0.0.0.0.0.0.0....."));
            sb.AppendLine(string.Format("  auther   : {0}", "takumi"));
            MessageBox.Show(sb.ToString(), this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
        }
        #endregion

        #region [control]
        private void buttonBrows_Click(object sender, RoutedEventArgs e)
        {
            openZipFile();
        }

        private void Grid_Drop(object sender, DragEventArgs e)
        {
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                string file = files[0];
                openZipFile(file);
                Message(string.Format("[MainWindow.Grid_Drop()] succeed"));
            }
            catch (Exception ex)
            {
                Message(string.Format("[MainWindow.Grid_Drop()] failed... {0}", new object[] { ex.ToString() }));
            }
        }

        #endregion


        #region INotifyPropertyChanged メンバー

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
}
