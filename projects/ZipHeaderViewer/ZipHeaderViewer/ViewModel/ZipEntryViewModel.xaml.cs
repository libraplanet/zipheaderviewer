﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZipHeaderViewer.ViewModel
{
    /// <summary>
    /// ZipEntryViewModel.xaml の相互作用ロジック
    /// </summary>
    public partial class ZipEntryViewModel : UserControl
    {
        public ZipEntryViewModel()
        {
            InitializeComponent();
        }

        public void SetAttributeRow(string key, string value)
        {
            Grid attributGrid = gridAttributes;
            Grid gridIndex = new Grid();
            Grid gridKey = new Grid();
            Grid gridValue = new Grid();
            int row = attributGrid.RowDefinitions.Count;
            Func<string, TextAlignment, UIElement> createControl = (string s, TextAlignment alignment) =>
            {
                Border b = new Border();

                TextBlock textBlock = new TextBlock() { Text = s };
                textBlock.TextAlignment = alignment;
                textBlock.TextWrapping = TextWrapping.Wrap;
                b.Child = textBlock;

                b.Padding = new Thickness(5);
                b.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                b.Background = new SolidColorBrush(Color.FromArgb(70, 102, 102, 102));
                b.BorderThickness = new Thickness(1);
                return b;
            };

            gridIndex.Children.Add(createControl(string.Format("{0}", row), TextAlignment.Right));
            gridKey.Children.Add(createControl(key, TextAlignment.Left));
            gridValue.Children.Add(createControl(value, TextAlignment.Left));

            attributGrid.Children.Add(gridIndex);
            attributGrid.Children.Add(gridKey);
            attributGrid.Children.Add(gridValue);
            attributGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Auto) });

            Grid.SetColumn(gridIndex, 0);
            Grid.SetColumn(gridKey, 1);
            Grid.SetColumn(gridValue, 2);
            Grid.SetRow(gridIndex, row);
            Grid.SetRow(gridKey, row);
            Grid.SetRow(gridValue, row);
        }
    }
}
