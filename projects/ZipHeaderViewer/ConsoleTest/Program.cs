﻿using System;
using System.IO;
using System.Text;

namespace ConsoleTest
{
    using ZipHeaderViewerLib.Lib.IO;

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hellow ZipCS!");
            
            try
            {
                StraightReader r = new StraightReader();
                using (FileStream fStream = new FileStream("test.zip", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (BufferedStream bStream = new BufferedStream(fStream))
                {
                    r.Read(bStream);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("appear exception!");
                Console.WriteLine(e);
            }
        }
    }
}
