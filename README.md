# これは？ #

ZIP ヘッダーを簡易的にみるビューアです。

PKZIPのAPPNOTEを元に、ヘッダーのみ出力しています。

## なんのために？ ##

世の中にユーティリティは数多くあります。

しかし、ヘッダーをプレーンに出力してくれるツールが無かったため

とりあえず作ってみました。

## で、本当はなんのため？ ##

はい。

LanguageEncodingFlagを簡単に見たかったので作りました。

とりあえず、頭からヘッダをなめて一覧に表示しているだけです。

α未満のため、本リポジトリにおける全てに一切の責任を持ちません。


# SS #

![SS_v0.0.0.0.0.0.0.0.2.png](https://bitbucket.org/libraplanet/zipheaderviewer/raw/3fc740623239f01dfc1f479d4ecbbc36ab065857/images/v0.0.0.0.0.0.0.0.2.png)

# ZIPの文字化けについて #

数多くの文献でZIPファイルの対処法を説明してくれています。

しかし、そのほとんどが、本質についての解説がありません。

ZIPの文字化けをバグの様に説明している文献も多数あります。

しかし、それは誤りです。一口にバグとは言い切れません。

## 旧仕様のZIP ##

ZIPの元々の仕様には文字コードについての言及はなく

OSで規定されているシステム文字コードで扱います。

そのため、圧縮環境と解凍環境で文字コードが一致している必要があります。

これは、ZIPに限らず数多くのUNICODEに対応していないファイル フォーマットが同様です。


## UNICODE対応仕様のZIP ##

旧来の仕様では文字コードがシステムに依存して解凍を保証することが難しいため、UNICODEへの対応が仕様に追加されました。

(APPNNOTE 6.3.0)

* General Purpose Bit Flagの11bit目、Language encoding flag (EFS)が立っている時
* ファイル名やコメントはUTF-8

この改定により、文字コードの規定が追加されたことで、システム文字コードに依存しない読み取りが可能になりました。


## Language encoding flag (EFS) ##

Language encoding flagは常に立てることが求められているわけではありません。

ファイルのEntryごとにフラグがあり、1つのZIPの中でそれぞれに指定が可能です。

文字列の中身がASCIIのみで構成されているのであれば立てる必要はありません。

しかし、UTF-8のASCII部分は互換性があるため、常に立てても問題はありません。


Entry|Language encoding flag|圧縮字の文字コード|解凍時の文字コード
--|--|--|--
UNICODE非対応|立てない(0)|圧縮するOSのシステム文字コード|圧縮するOSのシステム文字コード
UNICODE対応|立てる(1)|UTF-8|UTF-8|

※Language encoding flagを立てることで圧縮時/解凍時の文字コードがUTF-8に統一されるため文字化けを解消できるという発想です。

※UTF-8のときにLanguage encoding flagを立てなければいけない訳ではありません。


# よくある文字化ける原因 #

## 圧縮 ##

### Java6 ###
Java6(JDK6)のZIPはUTF-8で圧縮を行いますが、Language encoding flagを立てません。

(旧仕様で単にUTF-8が用いられているだけ)

そのため、通常の解凍ソフトではOSのシステム文字コードで解凍をかけるため、

特にWindowsでは文字化けが発生します。

この場合、これはバグではなく、UTF-8でないシステムで解凍する行為に誤りがあります。

また、WindowsなどのUTF-8がシステム文字コードでないシステムでの解凍が想定される場合、

Apache Commons Compressなど、ほかのライブラリを使用する必要があります。

## 解凍 ##

### Windows7 Explorer ###

Windows7 ExplorerはUNICODE対応されたZIPの解凍に対応していません。

Language encoding flagを評価しないため、常にシステム 文字コードで解凍します。

[KB2704299](https://support.microsoft.com/ja-jp/kb/2704299)の修正をあてることでUNICODE対応されたZIPの解凍に対応します。


尚、Windows8以降はデフォルトで対応しています。



# 総括 #

文字化けの要因は様々あります。

圧縮側に問題があることもあれば、解凍側に問題があることもあります。

文字化けをした場合、単に化けたと喚き散らすのではなく、

圧縮されたZIPが正しいのか、解凍ソフトはUNICODEに対応しているのか

判断が必要です。

また、UNICODEには正規化(Normalized Form)があります。

殆どのシステムはNFCですが、Macは一部NFDを用います。

NFDもUNICODEの一部であり、UTF-8で用いられることに間違いはありません。

NFDが化ける場合は解凍ツールのUNICODEへの対応不足となりますので、

必要があれば別の解凍ツールの検討をしましょう。

なお、国際文字対応を考えている場合、Language encoding flagを立ててUTF-8を用いるのが無難かと思います。


# thanx #

[・PKZIP APPNOTE](https://pkware.cachefly.net/webdocs/casestudies/APPNOTE.TXT)

[・ZIP (ファイルフォーマット) - Wikipedia:](https://ja.wikipedia.org/wiki/ZIP_(%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB%E3%83%95%E3%82%A9%E3%83%BC%E3%83%9E%E3%83%83%E3%83%88))

[・Zip (file format) - Wikipedia:](https://en.wikipedia.org/wiki/Zip_(file_format))

[・java.util.zip (Java Platform SE 6):](https://docs.oracle.com/javase/jp/6/api/java/util/zip/package-summary.html)

[・org.apache.commons.compress.archivers.zip (Apache Commons Compress 1.12 API):](https://commons.apache.org/proper/commons-compress/javadocs/api-1.12/org/apache/commons/compress/archivers/zip/package-summary.html)

過去にZIP実装会にて様々な知識を与えてくれた @7shi さん、 @awazo さんなどに多大なる感謝。